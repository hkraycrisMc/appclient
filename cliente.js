// Cliente HTTP
//github.com/users/raymrsite
const fs = require('fs');
class Client {

      constructor(host, port,protocol){
        this.host = host;
        this.port = port;
        this.protocol = protocol;

        if(protocol != "http" && protocol != "https"){
          console.log("ERROR!!!");
         }

         if (!fs.existsSync('/tmp')) {
           fs.mkdir('/tmp', err => console.error(err));
         }

          this.logDir =  fs.mkdtempSync('/tmp/foo-');
      }
      //process headers to support session -> se realiza en la perticion o request.

      autenticarBasic(user, pass){
                       this.basicAuth = new Buffer(user + ":" + pass).toString("base64");
      }

      procesarHeaders(){
        var headers = {
            "Accept": "*/*",
            "User-Agent":"Cliente Node.js"
        };
        if( this.basicAuth != undefined){
          headers.Authorization = "Basic " + this.basicAuth;
        }
         return headers;
      }
          //Realizar peticiones HTTP de tipo Get (obtener informacion)
      get(uri, callback){
           var options = {
             hostname: this.host,
             port: this.port,
             method: 'GET',
             path: this.protocol + "://" + this.host + uri,
             headers: this.procesarHeaders()
           }

           this.request(options,null, callback);
      }

      post(uri, data, callback){

        var options = {
          hostname: this.host,
          port: this.port,
          method: 'POST',
          path: this.protocol + "://" + this.host + uri,
          headers: this.procesarHeaders()
        };
        this.request(options, data, callback);
      }

      request(options,data,  callback){
          //http o https
          var http = require(this.protocol); // http, o https

          var response = {
            status:null,
            body:"",
            headers:null
          };
          let request = http.request(options, (chanelResponse) => {
                 chanelResponse.on("data", (chunk) => {
                        response.body += chunk;
                 });
                 chanelResponse.on("end", ()=>{
                   response.status = chanelResponse.statusCode;
                   response.headers = chanelResponse.headers;
                   fs.appendFile(this.logDir+"/cliente.log",JSON.stringify(response), () =>{ return null;});
                   callback(response);

                 });
          });

          if ( data != undefined && data != null){
               request.write(JSON.stringify(data));
          }
          request.end();
      }
}

module.exports = Client;
